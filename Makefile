PACKAGE := github.com/xunien/attachy

VERSION_VAR  := main.VersionString
REPO_VERSION := $(shell git describe --always --dirty --tags)

REV_VAR  := main.RevisionString
REPO_REV := $(shell git rev-parse -q HEAD)

GO  ?= go
GOX ?= gox
GOBUILD_LDFLAGS := -ldflags "\
	-X '$(VERSION_VAR)=$(REPO_VERSION)' \
	-X '$(REV_VAR)=$(REPO_REV)' \
"
GOBUILD_FLAGS ?=
GOTEST_FLAGS  ?=
GOX_OSARCH    ?= linux/amd64 linux/arm
GOX_FLAGS     ?= -output="builds/attachy_{{.OS}}_{{.Arch}}" -osarch="$(GOX_OSARCH)" -parallel=3

TRAVIS_BUILD_DIR ?= .
export TRAVIS_BUILD_DIR

.PHONY: all
all: clean test

.PHONY: test
test: build fmt

.PHONY: build
build: deps .build

.PHONY: .build
.build:
	$(GO) build $(GOBUILD_FLAGS) $(GOBUILD_LDFLAGS) ./...

.PHONY: crossbuild
crossbuild: deps
	$(GOX) $(GOX_FLAGS) $(GOBUILD_FLAGS) $(GOBUILD_LDFLAGS) ./...

.PHONY: deps
deps: .gox-install

.gox-install:
	$(GO) get -x github.com/mitchellh/gox

.PHONY: distclean
distclean: clean
	$(RM) -rv ./builds

.PHONY: clean
clean:
	$(GO) clean -x ./... || true

.PHONY: fmt
fmt:
	set -e; for f in $(shell git ls-files '*.go'); do gofmt $$f | diff -u $$f - ; done
