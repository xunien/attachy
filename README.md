[![Build Status](https://travis-ci.org/xunien/attachy.svg?branch=master)](https://travis-ci.org/xunien/attachy)

# attachy

attachy is a simple imap watcher that save message attachments.

## How to use

Copy config.yml.example and customize it as you wish

## Build and launch

```
go build
./attachy -configPath=./config.yml
```
