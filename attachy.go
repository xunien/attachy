// +build linux

package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"net/mail"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/mxk/go-imap/imap"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v2"
)

// Config represents attachy config file
type Config struct {
	Host            string `yaml:"host"`
	User            string `yaml:"user"`
	MBox            string `yaml:"mbox"`
	MimeType        string `yaml:"mime_type"`
	From            string `yaml:"from"`
	DestinationPath string `yaml:"destination_path"`
}

const (
	// idle every 20 minutes (RFC-2177 recommends 29 mins max)
	IdleTimeout = 20 * time.Minute
)

var logger = logrus.New()

func main() {
	configPath := flag.String("configPath", "./config.yml", "path of the configuration file")
	flag.Parse()

	logger.Level = logrus.DebugLevel
	logger.Out = os.Stderr
	logger.Formatter = &logrus.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	}

	config, err := loadConfig(configPath)
	if err != nil {
		logger.Fatal(err)
	}

	imap.DefaultLogger = log.New(os.Stdout, "", 0)
	imap.DefaultLogMask = imap.LogConn | imap.LogCmd | imap.LogState

	client, err := dial(config.Host)
	if err != nil {
		logger.Fatal(err)
	}

	defer client.Close(false)

	if client.Caps["STARTTLS"] {
		client.StartTLS(nil)
	}

	password, err := readPassword()
	if err != nil {
		logger.Fatalf("Unable to read password: %s", err.Error())
	}

	if _, err := login(client, config.User, password); err != nil {
		logger.Fatalf("Unable to login: %s", err.Error())
	}

	if _, err := client.Select(config.MBox, false); err != nil {
		logger.Fatalf("Unable to select %s: %s", config.MBox, err.Error())
	}

	// force select on start
	if err := loadMessages(client, config); err != nil {
		logger.Error(err)
	}

	// setup interrupt signal channel to terminate the idle
	interrupt := make(chan os.Signal, 1)

	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	timeout := time.NewTicker(IdleTimeout)

	// tick every 5s to poll imap socket
	sleeper := time.NewTicker(5 * time.Second)

	// setup poller signal for checking for data on the idle command
	poll := make(chan bool, 1)
	poll <- true

	logger.Info("Beginning idle...")
	if _, err := client.Idle(); err != nil && err != imap.ErrTimeout {
		logger.Fatalf("Idle error: %s", err.Error())
		return
	}

	for {
		select {
		case <-sleeper.C:
			poll <- true

		case <-poll:

			if err := client.Recv(0); err != nil && err != imap.ErrTimeout {
				logger.Errorf("Idle error: %s", err.Error())
				continue
			}

			for _, data := range client.Data {
				if imap.Data == data.Type {
					logger.Infof("Received an %s notification", data.Label)

					if data.Label != "EXISTS" {
						client.Data = nil
						break
					}

					// temporarily pause the idle so we can fetch the message
					if _, err = client.IdleTerm(); err != nil {
						logger.Errorf("Unable to temporarily pause the idle: %s", err.Error())
						client.Data = nil
						break
					}

					logger.Info("Terminated idle. Looking for new messages...")

					err := loadMessages(client, config)
					if err != nil {
						logger.Error(err)
					}

					logger.Info("Continuing idle...")

					// turn idle back on
					if _, err = client.Idle(); err != nil {
						logger.Errorf("Unable to restart idle: %s", err.Error())
					}

					client.Data = nil
				}
			}

		case s := <-interrupt:
			switch s {
			case syscall.SIGHUP:
				if _, err := client.IdleTerm(); err != nil {
					logger.Errorf("Unable to temporarily pause the idle: %s", err.Error())
					return
				}

				logger.Info("Terminated idle. Looking for new messages...")

				err := loadMessages(client, config)
				if err != nil {
					logger.Error(err)
				}

				logger.Info("Continuing idle...")

				if _, err := client.Idle(); err != nil {
					logger.Errorf("Unable to restart idle: %s", err.Error())
					return
				}

			case syscall.SIGTERM, syscall.SIGINT:
				logger.Info("Received interrupt. Terminating idle...")
				if _, err := client.IdleTerm(); err != nil {
					logger.Errorf("Unable to terminate the idle: %s", err.Error())
				}
				return
			}
		case <-timeout.C:
			logger.Info("Resetting idle...")
			_, err := client.IdleTerm()
			if err != nil {
				logger.Info("error while temporarily terminating idle: %s", err.Error())
				return
			}

			logger.Info("Terminated idle")

			_, err = client.Idle()
			if err != nil {
				logger.Info("Unable to restart idle: %s", err.Error())
				return
			}
			logger.Info("Idle restarted.")
		}
	}

	return
}

// loadConfig read config.yml file
func loadConfig(configPath *string) (*Config, error) {
	file, err := os.Open(*configPath)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	config := &Config{}

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(b, config); err != nil {
		return nil, err
	}

	if _, err := os.Stat(config.DestinationPath); os.IsNotExist(err) {
		return nil, err
	}

	return config, nil
}

// readPassword read password from stdin without printing it to stdout
func readPassword() (string, error) {
	fmt.Print("Password: ")

	b, err := terminal.ReadPassword(int(syscall.Stdin))

	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(b)), nil
}

// loadMessages will search and fetch message body
func loadMessages(client *imap.Client, config *Config) error {
	cmd, err := imap.Wait(client.UIDSearch("FROM", client.Quote(config.From), "UNSEEN"))
	if err != nil {
		return err
	}

	seq := new(imap.SeqSet)

	if len(cmd.Data) > 0 {
		seq.AddNum(cmd.Data[0].SearchResults()...)
	}

	if seq.Empty() {
		return nil
	}

	logger.Infof("Fetched UIDs %v", seq)

	cmd, err = imap.Wait(client.UIDFetch(seq, "FLAGS", "INTERNALDATE", "RFC822.SIZE", "BODY[]"))
	if err != nil {
		return err
	}

	for _, rsp := range cmd.Data {
		body, err := fetchMessage(client, rsp.MessageInfo().UID)

		if err != nil {
			logger.Fatal(err)
			return err
		}

		err = writeFileFromBody(bytes.NewReader(body), config)

		if err != nil {
			logger.Fatal(err)
			return err
		}
	}

	if !seq.Empty() {
		_, err = imap.Wait(client.UIDStore(seq, "+FLAGS.SILENT", imap.NewFlagSet(`\Seen`)))
	}

	cmd.Data = nil

	return err
}

// writeFileFromBody parse and save desired part from multipart
func writeFileFromBody(body io.Reader, config *Config) error {
	msg, err := mail.ReadMessage(body)

	if err != nil {
		return err
	}

	mediaType, params, err := mime.ParseMediaType(msg.Header.Get("Content-Type"))
	if err != nil {
		logger.Fatal(err)
		return err
	}

	if !strings.HasPrefix(mediaType, "multipart/") {
		return nil
	}

	mr := multipart.NewReader(msg.Body, params["boundary"])

	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		}

		slurp, err := ioutil.ReadAll(p)
		if err != nil {
			logger.Error(err)
			break
		}

		contentType := p.Header.Get("Content-Type")

		// Handle .eml files
		if strings.HasPrefix(contentType, "message/rfc822") {
			if err := writeFileFromBody(bytes.NewReader(slurp), config); err != nil {
				return err
			}
		}

		if !strings.HasPrefix(contentType, config.MimeType) {
			continue
		}

		filename, err := parseFilename(p)
		if err != nil {
			logger.Error(err)
			break
		}

		data, err := base64.StdEncoding.DecodeString(string(slurp))
		if err != nil {
			logger.Error(err)
			break
		}

		filename = path.Join(config.DestinationPath, filename)
		logger.WithField("filename", filename).Info("Writing file...")
		err = ioutil.WriteFile(filename, data, 0644)
		if err != nil {
			logger.Error(err)
			break
		}
	}

	return nil
}

// dial open imap socket
func dial(host string) (*imap.Client, error) {
	var client *imap.Client
	var err error

	if strings.HasSuffix(host, ":993") {
		client, err = imap.DialTLS(host, nil)
	} else {
		client, err = imap.Dial(host)
	}

	if err != nil {
		return nil, err
	}

	return client, nil
}

// sensitive disable imap logger to hidde sensitive data
func sensitive(client *imap.Client, action string) imap.LogMask {
	mask := client.SetLogMask(imap.LogConn)
	hide := imap.LogCmd | imap.LogRaw

	if mask&hide != 0 {
		client.Logln(imap.LogConn, "Raw logging disabled during", action)
	}

	client.SetLogMask(mask &^ hide)

	return mask
}

// login send LOGIN imap command
func login(client *imap.Client, user string, password string) (*imap.Command, error) {
	defer client.SetLogMask(sensitive(client, "LOGIN"))
	return client.Login(user, password)
}

// fetchMessage send UID FETCH command to retrieve message body
func fetchMessage(client *imap.Client, messageUID uint32) ([]byte, error) {
	seq := new(imap.SeqSet)

	seq.AddNum(messageUID)
	cmd, err := imap.Wait(client.UIDFetch(seq, "INTERNALDATE", "BODY[]", "UID", "RFC822.HEADER"))

	if err != nil {
		logger.Errorf("Unable to fetch message (%d): %s", messageUID, err.Error())
		return nil, nil
	}

	if len(cmd.Data) == 0 {
		logger.WithField("uid", messageUID).Info("Unable to fetch message from src: NO DATA")
		return nil, errors.New("message not found")
	}

	msgFields := cmd.Data[0].MessageInfo().Attrs
	body := imap.AsBytes(msgFields["BODY[]"])

	cmd.Data = nil

	return body, nil
}

// parseFilename returns the filename from Content-Type header
func parseFilename(p *multipart.Part) (string, error) {
	v := p.Header.Get("Content-Type")

	_, dispositionParams, err := mime.ParseMediaType(v)
	if err != nil {
		return "", err
	}

	return dispositionParams["name"], nil
}
